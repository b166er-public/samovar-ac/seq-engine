#
# Copyright (C) 2021 Daniil Moerman
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import numpy as np
import uuid

# -------------------------------------
# Public API
# -----------------------------------------------------------------------------

class SeqEngineException(Exception):
    def __init__(self, message=None):     
        if message is None:            
            super().__init__("No message had been passed!")
        else:
            super().__init__(message)

class Engine(object):
    def __init__(self, spb=32, bpm=120, global_seed=None):
        self._step = 0
        self._spb = spb
        self._bpm = bpm
        
        if global_seed is None:
            self._global_rng = np.random.default_rng()
        else:
            self._global_rng = np.random.default_rng(global_seed)
            
        self._components = dict()
        self._order = list()
        self._wires = dict() # UUID -> Range
        self._wires_read_count = dict() # UUID -> ReadCount
        
        self._step_event_wire_id = self._create_new_wire(1)
        self._beat_event_wire_id = self._create_new_wire(2)
        
    def get_step_event_wire_id(self):
        return self._step_event_wire_id
    
    def get_beat_event_wire_id(self):
        return self._beat_event_wire_id
    
    def get_all_wire_ids(self):
        return self._wires.keys()
    
    def get_wire_range(self, wire_id):
        if wire_id in self._wires:
            return int(self._wires[wire_id])
        else:
            raise SeqEngineException("The wire [{}] is unknown!".format(wire_id))
        
    def get_not_grounded_wire_list(self):
        result = set()
        for w in self._wires_read_count:
            if int(self._wires_read_count[w]) < 1:
                result.add(w)
        return result
    
    def is_wire_grounded(self, wire_id):
        return int(self._wires_read_count[wire_id]) > 0
    
    def _create_new_wire(self, evt_range):
        wire_id = str(uuid.uuid4())
        self._wires[wire_id] = evt_range
        self._wires_read_count[wire_id] = 0
        return wire_id
    
    def _increase_wire_read_count(self, wire_id):
        self._wires_read_count[wire_id] = int(self._wires_read_count[wire_id] + 1)
        
    def _next_global_rng_int(self):
        return self._global_rng.integers()
    
    def add_frame(self, evt_in_wire_id_list=None, activation_matrix_stack=None, variation_cycle=0):
        if evt_in_wire_id_list is None:
            raise SeqEngineException("Engine: Need trigger source for frame!")
        elif activation_matrix_stack is None:
            raise SeqEngineException("Engine: Need activation matrix stack for frame!")
        else:
            component_id = str(uuid.uuid4())
            
            sources = list(evt_in_wire_id_list)
            for s in sources:
                self._increase_wire_read_count(s)
                
            new_frame = _Frame(activation_matrix_stack)
            target = None
            cycle = variation_cycle
            self._components[component_id] = { "unit":new_frame, "sources":sources, "target":target, "cycle":cycle }
            self._order.append(component_id)
            
            
                
    def add_event_remapper(self, evt_in_wire_id_list=None, mapping_array=None):
        if evt_in_wire_id_list is None:
            raise SeqEngineException("Engine: Need event source for remapper!")
        elif mapping_array is None:
            raise SeqEngineException("Engine: Need mapping array for remapper!")
        else:
            component_id = str(uuid.uuid4())
            
            sources = list(evt_in_wire_id_list)
            for s in sources:
                self._increase_wire_read_count(s)
                
            target = self._create_new_wire(np.amax(mapping_array))
            new_remapper = _EventRemapper(mapping_array)
            self._components[component_id] = { "unit":new_remapper, "sources":sources, "target":target, "cycle":0 }
            self._order.append(component_id)
            
            return target
        
    def add_event_processor(self, evt_in_wire_id_list=None, transition_matrix_stack=None, output_matrix=None, transition_seed=None, output_seed=None, reset_cycle=0):
        if evt_in_wire_id_list is None:
            raise SeqEngineException("Engine: Need event source for processor!")
        elif transition_matrix_stack is None:
            raise SeqEngineException("Engine: Need transition matrix stack for processor!")
        elif output_matrix is None:
            raise SeqEngineException("Engine: Need output matrix for processor!")
        else:
            component_id = str(uuid.uuid4())
            
            sources = list(evt_in_wire_id_list)
            for s in sources:
                self._increase_wire_read_count(s)
                
            target = self._create_new_wire(output_matrix.shape[1])
            
            t_seed = transition_seed
            if t_seed is None:
                t_seed = self._next_global_rng_int()
            
            o_seed = output_seed
            if o_seed is None:
                o_seed = self._next_global_rng_int()
            
            new_processor = _EventProcessor(transition_matrix_stack, output_matrix, t_seed, o_seed)
            
            self._components[component_id] = { "unit":new_processor, "sources":sources, "target":target, "cycle":reset_cycle }
            self._order.append(component_id)
            
            return target
        
    def render(self, steps=1):
        result = np.zeros(shape=(steps,16,128), dtype=np.int32)
        
        for s in range(steps):
            current_step = self._step
            eval_ctx = {}
            eval_ctx[self.get_step_event_wire_id()] = 1
            if current_step % self._spb == 0:
                eval_ctx[self.get_beat_event_wire_id()] = 1
            else:
                eval_ctx[self.get_beat_event_wire_id()] = 0
            
            # Render step frame
            step_frame = np.zeros(shape=(16,128), dtype=np.int32)
            for c in self._order:
                component = self._components[c]
                
                # Collect sources
                sources = list()
                for src in component["sources"]:
                    sources.append(eval_ctx[src])
                
                if len(sources) == 1:
                    sources = int(sources[0])
                    
                # Perform action
                unit = component["unit"]
                if isinstance(unit, _EventRemapper):
                    output = unit.evaluate(sources)
                    eval_ctx[component["target"]] = output
                elif isinstance(unit, _Frame):
                    var_cycle = component["cycle"]
                    if (var_cycle > 0) and ((current_step % var_cycle) == 0):
                        unit.next_variation()
                    output_frame = unit.evaluate(sources)
                    step_frame += output_frame
                    
                elif isinstance(unit, _EventProcessor):
                    reset_cycle = component["cycle"]
                    if (reset_cycle > 0) and ((current_step % reset_cycle) == 0):
                        unit.reset()
                    output = unit.evaluate(sources)
                    eval_ctx[component["target"]] = output
                else:
                    raise SeqEngineException("Engine: Unknown unit type [{}] !".format(str(type(unit))))
                    
            # Write frame to result
            result[s,:] = step_frame

            # Increase step counter
            self._step = self._step + 1
                
        return result
    
# -------------------------------------
# Private API 
# -----------------------------------------------------------------------------

class _EventRemapper(object):
    def __init__(self, mapping_array=None):
        if mapping_array is None:
            raise SeqEngineException("EventRemapper: Need a mapping array!")
        else:
            self._mapping_array = mapping_array.copy()
            self._out_range = np.amax(self._mapping_array) + 1
            
    def get_event_input_range_list(self):
        return list(self._mapping_array.shape)
    
    def get_event_output_range(self):
        return int(self._out_range)
    
    def evaluate(self,evt_in_list = None):
        if evt_in_list is None:
            raise SeqEngineException("EventRemapper: Input needed for evaluation!")
        else:
            return int(self._mapping_array[evt_in_list]) % (self._out_range)
        
class _Frame(object):
    def __init__(self, activation_matrix_stack=None):
        if activation_matrix_stack is None:
            raise SeqEngineException("Frame: Need at least an activation matrix!")
        else:
            self._activation_matrix_stack = activation_matrix_stack.copy()
            self._variation_index = 0
            
    def is_single_variation(self):
        return len(self._activation_matrix_stack.shape) < 3
    
    def get_number_of_variations(self):
        if self.is_single_variation():
            return 1
        else:
            return int(self._activation_matrix_stack.shape[0])
        
    def next_variation(self):
        if not self.is_single_variation():
            variation_range = self.get_number_of_variations()
            self._variation_index = (self._variation_index + 1) % variation_range
        else:
            pass # NoOp as only one variation had been provided
            
    def evaluate(self,evt_in_list = None):
        if evt_in_list is None:
            return np.zeros(shape=(16,128), dtype=np.int32)
        elif np.amax(np.array(evt_in_list)) < 1:
            return np.zeros(shape=(16,128), dtype=np.int32)
        else:
            return self._activation_matrix_stack[self._variation_index,:,:].copy()
        
class _EventProcessor(object):
    def __init__(self, transition_matrix_stack=None, output_matrix=None, transition_seed=None, output_seed=None):
        if transition_matrix_stack is None:
            raise SeqEngineException("EventProcessor: Please provide a transion matrix for every input event!")
        elif output_matrix is None: 
            raise SeqEngineException("EventProcessor: Please provide an output matrix!")
        elif transition_seed is None: 
            raise SeqEngineException("EventProcessor: Please provide a seed value for transition random generator!")
        elif output_seed is None:
            raise SeqEngineException("EventProcessor: Please provide a seed value for output random generator!")
        else:
            self._transition_matrix_stack = transition_matrix_stack
            self._output_matrix = output_matrix
            self._normalize_transition_matrix_stack()
            self._normalize_output_matrix()
            self._transition_seed = transition_seed
            self._output_seed = output_seed
            
            self._current_state = 0
            self._rng_transition = np.random.default_rng(self._transition_seed)
            self._rng_output = np.random.default_rng(self._output_seed)
            
    def _normalize_transition_matrix_stack(self):
        tms = self._transition_matrix_stack
        for evt_in in range(tms.shape[0]):
            for state_from in range(tms.shape[1]):
                normalized_transition = (tms[evt_in,state_from] / tms[evt_in,state_from].sum())
                tms[evt_in,state_from] = normalized_transition
    
    def _normalize_output_matrix(self):
        om = self._output_matrix
        for state_id in range(om.shape[0]):
            normalized_emission = (om[state_id] / om[state_id].sum())
            om[state_id] = normalized_emission
    
    def get_event_intput_range(self):
        return self._transition_matrix_stack.shape[0]
    
    def get_event_output_range(self):
        return self._output_matrix.shape[1]
    
    def get_number_of_states(self):
        return self._output_matrix.shape[0]
    
    def get_state(self):
        return self._current_state
    
    def reset_state(self):
        self._current_state = 0
        
    def reset_transition_rng(self):
        self._rng_transition = np.random.default_rng(self._transition_seed)
    
    def reset_output_rng(self):
        self._rng_output = np.random.default_rng(self._output_seed)
        
    def reset(self):
        self.reset_state()
        self.reset_transition_rng()
        self.reset_output_rng()
        
    def evaluate(self, evt_in = None):
        if evt_in is None:
            raise SeqEngineException("EventProcessor: Need input event for evaluation!")
        else:
            tm = self._transition_matrix_stack[evt_in,:,:]
            tv = tm[self._current_state,:]
            card_state_set = int(self.get_number_of_states())
            new_state = int(self._rng_transition.choice(card_state_set, p=tv))
            ov = self._output_matrix[new_state,:]
            card_output_set = int(self.get_event_output_range())
            new_output = int(self._rng_output.choice(card_output_set, p=ov))
            
            self._current_state = new_state
            return new_output
    
# seq-engine

This project hold the implementation of algorithm composition engine.

## Some notes for later

**Object model**

EventProcessor
  - Event Input Wire ID (int)
  - Event Output Wire ID (int)
  - Transition Matrix Stack ( card(EventInSet) x card(StateSet) x card(StateSet) )
  - Output Matrix ( card(StateSet) x card(StateSet) )
  - Transition Seed (int)
  - Output Seed (int)
  - Reset State Cycle (0 == no reset)
  - Reset Transition Cycle (0 == no reset)
  - Reset Output Cycle (0 == no reset)

EventRemapper
  - Event Input Wire ID List ( list[int] )
  - Event Output Wire ID (int)
  - Mapping Array ( card(InEvt1) x ... x card(InEvtN) ) 

Frame
  - Event Input Wire ID List ( list[int] )
  - Activation Matrix Stack ( variants x 16 x 128 )
  - Variation Step Cycle (int)


**API (Public)**

SeqEngine(spb, bpm, seed)
  - get_step_event_wire_id() -> int
  - get_beat_event_wire_id() -> int
  - add_event_processor(evt_in_wire_id, params) -> evt_out_stream_id
  - add_event_remapper(evt_in_wire_id_list, params) -> evt_out_stream_id
  - add_frame(evt_in_wire_id_list, params) -> None
  - get_wire_list() -> list[int]
  - get_wire_range(int) -> int
  - get_not_grounded_wire_list() -> list[int]
  - is_wire_grounded(int) -> bool
  - render(steps) -> array[steps, 16, 128]

**API (Private)**

Wire(range)
  - set_value(int) -> None
  - get_value() -> int
  - reset_value() -> None

EventProcessor(t_ms, o_m, t_seed, o_seed)
  - get_event_intput_range() -> int
  - get_event_output_range() -> int
  - get_state_id() -> int
  - reset_state() -> None
  - reset_transition_rng() -> None
  - reset_output_rng() -> None
  - evaluate(int) -> int

EventRemapper(mapping_array, out_range)
  - get_event_input_range_list() -> list[int]
  - get_event_output_range() -> int
  - evaluate(list[int]) -> int

Frame(ams)
  - get_variation_range() -> int
  - move_to_next_variation() -> None
  - evaluate() -> ( 16 * 128 )
